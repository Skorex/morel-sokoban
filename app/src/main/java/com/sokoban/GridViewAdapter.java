package com.sokoban;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GridViewAdapter extends BaseAdapter
{
    Context c;
    String items[];

    GridViewAdapter(Context c, String arr[])
    {
        this.c = c;
        items = arr;
    }

    @Override public int getCount()
    {
        return items.length;
    }

    @Override public Object getItem(int i)
    {
        return null;
    }

    @Override public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_layout, null);
        }
        //Log.d("MOREL5", items[i]);

        ImageView imageView = view.findViewById(R.id.imageView);
        imageView.setImageResource(Sprite.getSprite(items[i].charAt(0)));

        return view;
    }
}
