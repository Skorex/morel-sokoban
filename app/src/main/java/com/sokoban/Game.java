package com.sokoban;

public class Game {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String[] getMap() {
        return map;
    }

    public int getLength() {
        return length;
    }

    private String[] map;
    private int length;

    Game(int id, String name, String[] map, int length) {
        this.id = id;
        this.name= name;
        this.map = map;
        this.length = length;
    }


}
