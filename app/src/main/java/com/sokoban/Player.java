package com.sokoban;

import android.util.Log;

enum Movement { TOP, RIGHT, BOTTOM, LEFT };

public class Player {
    private int x;
    private int y;
    private Board board;
    Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Player () {}

    private void setX(int x) { this.x = x; }
    private void setY(int y) { this.y = y; }

    public void setPos(int x, int y) {
        setX(x);
        setY(y);
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public boolean moove_top() {
        if (y-1 < 0) return false;
        boolean r = board.playerTo(x, y, x, --y, Movement.TOP);
        if (!r) y++;
        return r;
    }
    public boolean moove_right() {
        if (x+1 > board.getWidth()) return false;
        boolean r = board.playerTo(x, y, ++x, y, Movement.RIGHT);
        if (!r) x--;
        return r;
    }
    public boolean moove_bottom() {
        if (y+1 > board.getHeight()) return false;
        boolean r = board.playerTo(x, y, x, ++y, Movement.BOTTOM);
        if (!r) y--;
        return r;
    }
    public boolean moove_left() {
        if (x-1 < 0) return false;
        boolean r = board.playerTo(x, y, --x, y, Movement.LEFT);
        if (!r) x++;
        return r;
    }

    public boolean moove(Movement move) {
        switch (move) {
            case TOP    : return moove_top();
            case RIGHT  : return moove_right();
            case BOTTOM : return moove_bottom();
            case LEFT   : return moove_left();
        }
        return false;
    }
}
