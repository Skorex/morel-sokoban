package com.sokoban;

public enum CellType {
    BOX('C'), WALL('#'), EMPTY('.'), ENDPOINT('X'), PLAYER('P');

    public char abreviation;

    CellType(char c) {
        this.abreviation = c;
    }

    public char getAbreviation() {
        return abreviation;
    }

    public static CellType charToEnum(char c) {
        switch (c) {
            case 'C': return BOX;
            case '#': return WALL;
            case 'X': return ENDPOINT;
            case 'P': return PLAYER;
            default : return EMPTY;
        }
    }
}
