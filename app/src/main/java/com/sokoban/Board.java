package com.sokoban;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.Map;

public class Board {
    private static int lastid = 0;
    private static int maxlook = 2;

    private Game game;
    private int id;
    private int WITDH;
    private int HEIGHT;
    private int boxesForWin;
    private int hits;
    private CellType[][] map;
    private CellType[][] winmap;
    private Player player;
    private boolean win = false;


    public int getWidth() {
        return this.WITDH;
    }
    public int getHeight() {
        return this.HEIGHT;
    }
    public void addHit() {
        this.hits++;
    }
    public int getHits() { return this.hits; }
    public Game getGame() {  return game; }

    public void setGame(Game game) { this.game = game; }

    Board(int w, int h, String[] m, Player player) {
        this.id = lastid++;
        this.WITDH = w;
        this.HEIGHT = h;
        this.player = player;
        loadmap(m);
        //this.boxesForWin = 1; //definir en fonction de la map
        this.boxesForWin = getEndPoint();
        this.hits = 0;
    }
    Board(int w, int h, String[] m, Player player, Game g) {
        this.id = lastid++;
        this.WITDH = w;
        this.HEIGHT = h;
        this.player = player;
        loadmap(m);
        //this.boxesForWin = 1; //definir en fonction de la map
        this.boxesForWin = getEndPoint();
        this.hits = 0;
        this.game = g;
    }
    // Met dans this.map la map sous forme string[] s = "###", "P#.", "...", "..#"
    public void loadmap(String[] m) {
        this.winmap = new CellType[HEIGHT][WITDH];
        CellType[][] newmap = new CellType[HEIGHT][WITDH];
        int i = 0,j = 0;
        for (String line : m) {
            String[] c = line.split("");
            j = 0;
            for (String v : c) {
                char k = v.charAt(0);
                newmap[i][j] = CellType.charToEnum(k);
                winmap[i][j] = CellType.EMPTY;
                if (CellType.charToEnum(k) == CellType.PLAYER) player.setPos(j, i);
                if (newmap[i][j] == CellType.ENDPOINT) winmap[i][j] = newmap[i][j];
                j++;
            }
            i++;
        }
        this.map = newmap;
    }

    public int getEndPoint() {
        int cnt = 0;
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WITDH; j++) {
                if (this.winmap[i][j] == CellType.ENDPOINT) cnt++;
            }
        }
        return cnt;
    }
    public int nbBoxOK() {
        int cnt = 0;
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WITDH; j++) {
                if (this.winmap[i][j] == CellType.ENDPOINT && this.map[i][j] == CellType.BOX) cnt++;
            }
        }
        return cnt;
    }
    public boolean isWin() {
        if (nbBoxOK() == this.boxesForWin) return true;
        return false;
    }
    public void winner(Player player, TextView tvWin) {
        this.win = true;
        tvWin.setText("BRAVO TU AS GAGNE EN " + getHits() + " COUPS");
    }
    public void reload(TextView tvWin) {
        this.win = false;
        this.boxesForWin = getEndPoint();
        this.hits = 0;
        tvWin.setText("");
    }

    public int[]  incToDirection(Movement direction, int x, int y, int moveto) {
        int[] nextpos = {x, y};
        switch (direction) {
            case TOP    : nextpos[1]-=moveto; break;
            case RIGHT  : nextpos[0]+=moveto; break;
            case BOTTOM : nextpos[1]+=moveto; break;
            case LEFT   : nextpos[0]-=moveto; break;
        }
        return nextpos;
    }
    public boolean checkToTwo(int x, int y, CellType item1) {
        CellType item2 = map[y][x];
        switch (item1) {
            case BOX:
                Log.d("MOREL", "BOX");
                System.out.println("MORELO" + item2);
                if (item2 != CellType.BOX && item2 != CellType.WALL) { Log.d("MOREL", "BOX | BOX or WALL"); return true; }
            case WALL:
                return false;
            default:
                return true;
        }
    }
    public boolean checkToOne(int x, int y, Movement direction) {
        int[] nextpos = {x, y};
        CellType item1 = map[y][x];
        if (item1 == CellType.WALL)  { Log.d("check", "WALLL"); return false; }
        Log.d("check", "item1: " + item1);

        /*
        switch (direction) {
            case TOP    : nextpos[1]--; break;
            case RIGHT  : nextpos[0]++; break;
            case BOTTOM : nextpos[1]++; break;
            case LEFT   : nextpos[0]--; break;
        }
        */

        nextpos = incToDirection(direction, x, y, 1);

        return checkToTwo(nextpos[0], nextpos[1], item1);
    }

    // not used
    public int checkToNext(int x, int y, Movement direction, int index) {
        if (index >= maxlook) return 0;
        int res = 0;
        int[] nextpos = {x, y};
        CellType item1 = map[y][x];
        CellType item2 = null;
        if (index == 0 && item1 == CellType.WALL) return 0;
        //if (item1 == CellType.WALL) return false;

        nextpos = incToDirection(direction, x, y, 0);
        item2 = map[nextpos[1]][nextpos[0]];
        Log.d("MOREL2", "item1 : " + item1.toString() + " && item2 : " + item2.toString() + " => index : " + index);

        switch (item1) {
            case EMPTY    :
            case ENDPOINT : res = index + 1; break;
            case PLAYER   :
            case WALL     : res = 0; break;
            case BOX      :
                //Log.d("MOREL2", "index : " + index);
                //index++;
                //checkToNext(nextpos[0], nextpos[1], direction, index);
                res = index + 1;
                break;
                //if (index == maxlook-1 && (item2 != CellType.WALL && item2 != CellType.BOX)) { res = index + 1; break; }
        }
        Log.d("MOREL2", "return Eee : " + res);

        return res;
    }

    public boolean playerTo(int fromx, int fromy, int tox, int toy, Movement direction) {
        if (checkToOne(tox, toy, direction) == false) return false;
        if (this.win == true) return false;

        int[] nextpos = {tox, toy};
        nextpos = incToDirection(direction, tox, toy, 1);

        CellType item1 = map[toy][tox];
        Log.d("MOREL3", tox + " tox/toy " + toy + "nextpose : => y : " + nextpos[1] + " x : " + nextpos[0]);

        switch (item1) {
            case BOX:
                map[nextpos[1]][nextpos[0]] = CellType.BOX;
                map[toy][tox] = CellType.PLAYER;
                map[fromy][fromx] = CellType.EMPTY;
                this.addHit();
                return true;
            case EMPTY:
                map[fromy][fromx] = map[toy][tox];
                map[toy][tox] = CellType.PLAYER;
                this.addHit();
                return true;
            case ENDPOINT:
                map[fromy][fromx] = CellType.EMPTY;
                map[toy][tox] = CellType.PLAYER;
                this.addHit();
                return true;
            default:
                return false;
        }
    }

    public void drawboard() {
        String external = "-----------\n\n";
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WITDH; j++) {
                if ((map[i][j] != CellType.BOX && map[i][j] != CellType.PLAYER) && winmap[i][j] == CellType.ENDPOINT) {
                    external += this.winmap[i][j].getAbreviation();
                    continue;
                }
                external += this.map[i][j].getAbreviation();
            }
            external += "\n";
        }

        Log.d("MOREL", external);
    }
    public void drawGridView(Context context, GridView gridView) {
        //int i = 0; int j = 0; int k = 0;
        int i = 0, j = 0, k = 0;
        String[] smap = new String[map.length * map[0].length];

        for (i = 0; i < map.length; i++) {
            for (j = 0; j < map[i].length; j++) {
                if ((map[i][j] != CellType.BOX && map[i][j] != CellType.PLAYER) && winmap[i][j] == CellType.ENDPOINT) {
                    smap[k] = String.valueOf(this.winmap[i][j].getAbreviation());
                    k++;
                    continue;
                }
                smap[k] = String.valueOf(map[i][j].getAbreviation());
                k++;
            }
        }

        /*
        for (i = 0; i < map.length; i++) {
            for (CellType c : map[i]) {
                smap[j] = String.valueOf(c.getAbreviation());
                j++;
            }
        }
         */

        GridViewAdapter adapter = new GridViewAdapter(context, smap);
        gridView.setAdapter(adapter);
    }
}
