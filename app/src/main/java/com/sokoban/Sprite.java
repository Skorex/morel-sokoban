package com.sokoban;

public class Sprite {
    public static int getSprite(char c) {
        switch (c) {
            //case 'C': return R.drawable.crate;
            //case '#': return R.drawable.wall;
            //case 'X': return R.drawable.star;
            //case '.': return R.drawable.grass;
            //case 'P': return R.drawable.player;
            case 'C': return R.drawable.box;
            case '#': return R.drawable.roof;
            case 'X': return R.drawable.button;
            case '.': return R.drawable.blue;
            case 'P': return R.drawable.robot;
            default : break;
        }
        return 0;
    }
}
