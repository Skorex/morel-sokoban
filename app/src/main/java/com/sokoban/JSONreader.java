package com.sokoban;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONreader {
    JSONreader() {}

    public List<Game> readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            return readMessagesArray(reader);
        } finally {
            reader.close();
        }
    }

    public List<Game> readMessagesArray(JsonReader reader) throws IOException {
        List<Game> messages = new ArrayList<Game>();

        reader.beginArray();
        while (reader.hasNext()) {
            messages.add(readMessage(reader));
        }
        reader.endArray();
        return messages;
    }

    public Game readMessage(JsonReader reader) throws IOException {
        int id = -1;
        String name = null;
        String[] map = null;
        int length = 0;

        reader.beginObject();
        while (reader.hasNext()) {
            String property = reader.nextName();
            String v;
            switch (property) {
                case "id": id = reader.nextInt(); break;
                case "map" :
                    if (reader.peek() != JsonToken.NULL) {
                        map = readStringArray(reader);
                    }
                    break;
                case "name": name = reader.nextString(); break;
                case "length": length = reader.nextInt(); break;
                default: break;
            }
        }
        reader.endObject();
        return new Game(id, name, map, length);
    }

    public String[] readStringArray(JsonReader reader) throws IOException {
        List<String> str = new ArrayList<String>();

        //reader.beginArray();
        reader.beginArray();
        while (reader.hasNext()) {
            str.add(reader.nextString());
        }
        reader.endArray();

        return str.toArray(new String[str.size()]);
    }

    public List<Double> readDoublesArray(JsonReader reader) throws IOException {
        List<Double> doubles = new ArrayList<Double>();

        reader.beginArray();
        while (reader.hasNext()) {
            doubles.add(reader.nextDouble());
        }
        reader.endArray();
        return doubles;
    }

}
