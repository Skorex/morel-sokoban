package com.sokoban;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    int index = 0;
    Game g = null;
    List<Game> gm = null;
    private String[] map;
    private GridView gridView;
    private Board board;
    private Button btnTop;
    private Button btnRight;
    private Button btnBottom;
    private Button btnLeft;
    private Button btnRefresh;
    private Button btnNextLvl;
    private TextView tvWin;

    public void draw(String[] map) {
        for (String line : map) {
            System.out.println(line + "\n");
        }
    }

    public String[] initView(String[] map) {
        int i = 0;
        int j = 0;
        String[] smap = new String[map.length * map[0].length()];
        for (i = 0; i < map.length; i++) {
            String tmp = map[i];
            //j = 0;
            for (char c : tmp.toCharArray()) {
                smap[j] = String.valueOf(c);
                j++;
            }
        }
        return smap;
    }

    public Game getGameData(int index) {
        Game g = null;
        try {
            InputStream inputstream = getAssets().open("game.json");
            JSONreader jr = new JSONreader();
            gm = jr.readJsonStream(inputstream);
            //map = gm.get(0).getMap();
            g = gm.get(index);

            System.out.println(map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  g;
    }
    public void reloadView(Game gdata, String[] m) {
        board.setGame(gdata);
        board.loadmap(m);
        board.reload(tvWin);
        GridViewAdapter adapter = new GridViewAdapter(getApplicationContext(), initView(m));
        gridView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTop = findViewById(R.id.btnTop);
        btnRight = findViewById(R.id.btnRight);
        btnBottom = findViewById(R.id.btnBottom);
        btnLeft = findViewById(R.id.btnLeft);
        btnRefresh = findViewById(R.id.btnRefresh);
        btnNextLvl = findViewById(R.id.btnNext);
        tvWin = findViewById(R.id.tvWin);
        map = new String[]{};

        /*
        Create file
         */
        // check if file exist before


        //=============

        /*
        * SET UP THE GAME
         */
        //String[] map = { "##########", "#........#", "#..X.....#", "#.....C..#", "#......P.#", "##########"};
        //String[] map = {"##########", "#.....X..#", "#..X..C..#", "#.....C..#", "#......P.#", "##########"};

        System.out.println(map);

        g = getGameData(index);
        map = g.getMap();


        if (map.length != 0 && g != null) {
            Player player = new Player();
            Board board = new Board(10, 6, map, player, g);
            this.board = board;
            player.setBoard(board);

            /*
             * SET UP THE VIEW
             */

            String[] smap = initView(map);

            gridView = (GridView) findViewById(R.id.gdvMap); // init GridView
            gridView.setNumColumns(map[0].length());
            GridViewAdapter adapter = new GridViewAdapter(getApplicationContext(), smap);
            gridView.setAdapter(adapter);


            btnRefresh.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (index == gm.size()-1 ) {
                        index = 0;
                        g = getGameData(index);
                        map = g.getMap();
                    }
                    reloadView(g, map);
                }
            });

            btnNextLvl.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String[] m = map;
                    Game gdata = g;
                    Log.d("Loader", index + " " + (gm.size()-1));
                    if (index < gm.size()-1 ) {
                        gdata = getGameData(++index);
                        m = gdata.getMap();
                        reloadView(gdata, m);
                        g = gdata;
                        map = m;
                    } else {
                        tvWin.setText("STOP ! C'était le dernier niveau :( !");
                    }

                }
            });

            btnTop.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    boolean m = player.moove_top();
                    board.drawboard();
                    board.drawGridView(getApplicationContext(), gridView);

                    if (board.isWin()) board.winner(player, tvWin);
                }
            });
            btnRight.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    player.moove_right();
                    board.drawboard();
                    board.drawGridView(getApplicationContext(), gridView);

                    if (board.isWin()) board.winner(player, tvWin);
                }
            });
            btnBottom.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    boolean m = player.moove_bottom();
                    board.drawboard();
                    board.drawGridView(getApplicationContext(), gridView);

                    if (board.isWin()) board.winner(player, tvWin);
                }
            });
            btnLeft.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    boolean m = player.moove_left();
                    board.drawboard();
                    board.drawGridView(getApplicationContext(), gridView);

                    if (board.isWin()) board.winner(player, tvWin);
                }
            });
        }
    }
}